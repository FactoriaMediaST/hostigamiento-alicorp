import spine from "./spine-webgl";

export default class render {
  constructor(canvas, json, atlas, width, height, animation) {
    this.canvas = canvas;
    this.json = json;
    this.atlas = atlas;
    this.width = width;
    this.width = width;
    this.height = height;
    this.animation = animation;
    this.lastFrameTime = Date.now() / 1000;
    this.stop = false;
  }

  create() {
    this.gl =
      this.canvas.getContext("webgl", { alpha: true }) ||
      this.canvas.getContext("experimental-webgl", { alpha: true });
    this.shader = spine.webgl.Shader.newTwoColoredTextured(this.canvas);
    this.batcher = new spine.webgl.PolygonBatcher(this.canvas);
    this.mvp = new spine.webgl.Matrix4();
    this.mvp.ortho2d(0, 0, this.canvas.width - 1, this.canvas.height - 1);

    this.skeletonRenderer = new spine.webgl.SkeletonRenderer(this.canvas);

    this.assetManager = new spine.webgl.AssetManager(this.canvas);
    let texture = this.json.replace("json", "png");
    this.assetManager.loadText(this.json);
    this.assetManager.loadTextureAtlas(this.atlas);
    // this.assetManager.loadTexture(texture);
    requestAnimationFrame(() => {
      this.load(this.json, this.atlas, texture, this.animation);
    });
  }

  load(json, atlas, texture, animation) {
    if (this.assetManager.isLoadingComplete()) {
      var data = this.loadSkeleton(atlas, animation, "default", texture, json);
      this.skeleton = data.skeleton;
      this.state = data.state;
      this.bounds = data.bounds;
      requestAnimationFrame(this.render.bind(this));
    } else {
      requestAnimationFrame(() => {
        this.load(json, atlas, texture, animation);
      });
    }
  }
  loadSkeleton(atlas, initialAnimation, skin, texture, json) {
    if (skin === undefined) skin = "default";
    // Load the texture atlas using name.atlas and name.png from the AssetManager.
    // The function passed to TextureAtlas is used to resolve relative paths.
    atlas = this.assetManager.get(atlas);
    // Create a AtlasAttachmentLoader, which is specific to the WebGL backend.
    var atlasLoader = new spine.AtlasAttachmentLoader(atlas);
    // Create a SkeletonJson instance for parsing the .json file.
    var skeletonJson = new spine.SkeletonJson(atlasLoader);
    // Set the scale to apply during parsing, parse the file, and create a new skeleton.
    var skeletonData = skeletonJson.readSkeletonData(
      this.assetManager.get(json)
    );
    this.skeleton = new spine.Skeleton(skeletonData);
    this.skeleton.setSkinByName(skin);
    var bounds = this.calculateBounds(this.skeleton);
    // Create an AnimationState, and set the initial animation in looping mode.
    var animationState = new spine.AnimationState(
      new spine.AnimationStateData(this.skeleton.data)
    );
    animationState.setAnimation(0, initialAnimation, true);
    animationState.addListener({
      event: function(trackIndex, event) {
        // console.log("Event on track " + trackIndex + ": " + JSON.stringify(event));
      },
      complete: function(trackIndex, loopCount) {
        // console.log("Animation on track " + trackIndex + " completed, loop count: " + loopCount);
      },
      start: function(trackIndex) {
        // console.log("Animation on track " + trackIndex + " started");
      },
      end: function(trackIndex) {
        // console.log("Animation on track " + trackIndex + " ended");
      }
    });
    // Pack everything up and return to caller.
    return { skeleton: this.skeleton, state: animationState, bounds: bounds };
  }
  calculateBounds(skeleton) {
    this.skeleton.setToSetupPose();
    this.skeleton.updateWorldTransform();
    var offset = new spine.Vector2();
    var size = new spine.Vector2();
    this.skeleton.getBounds(offset, size, []);
    return { offset: offset, size: size };
  }
  render() {
    var now = Date.now() / 1000;
    var delta = now - this.lastFrameTime;
    this.lastFrameTime = now;
    this.resize();

    // Update the MVP matrix to adjust for canvas size changes
    this.gl.clearColor(0, 0, 0, 0);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    // Apply the animation state based on the delta time.

    this.state.update(delta);
    this.state.apply(this.skeleton);
    this.skeleton.updateWorldTransform();
    // Bind the shader and set the texture and model-view-projection matrix.
    this.shader.bind();
    this.shader.setUniformi(spine.webgl.Shader.SAMPLER, 0);
    this.shader.setUniform4x4f(spine.webgl.Shader.MVP_MATRIX, this.mvp.values);
    // Start the batch and tell the SkeletonRenderer to render the active skeleton.
    this.batcher.begin(this.shader);

    // skeletonRenderer.premultipliedAlpha = premultipliedAlpha;
    this.skeletonRenderer.draw(this.batcher, this.skeleton);
    this.batcher.end();
    this.shader.unbind();
    // draw debug information

    requestAnimationFrame(this.render.bind(this));
  }
  stop() {
    this.stop = true;
  }

  resize() {
    var w = this.canvas.clientWidth;
    var h = this.canvas.clientHeight;
    var bounds = this.bounds;
    if (this.canvas.width != w || this.canvas.height != h) {
      this.canvas.width = w;
      this.canvas.height = h;
    }
    // magic
    var centerX = bounds.offset.x + bounds.size.x / 2;
    var centerY = bounds.offset.y + bounds.size.y / 2;
    var scaleX = bounds.size.x / this.canvas.width;
    var scaleY = bounds.size.y / this.canvas.height;
    var scale = Math.max(scaleX, scaleY) * 1.2;
    if (scale < 1) scale = 1;
    var width = this.canvas.width * scale;
    var height = this.canvas.height * scale;
    this.mvp.ortho2d(centerX - width / 2, centerY - height / 2, width, height);
    this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
  }
}
